# Dave's Unofficial Slackbuilt Kernels

This is an experimental system for automatic building and Slackware packaging
of vanilla stable Linux kernels.

Every time a new kernel of the 4.4 long term support or 4.17 series is released,
unofficial x86_64 generic (and i686 generic-smp) packages for Slackware are
automatically built and published within a few hours.  You can download these
unofficial packages from https://dusk.idlemoor.tk/

But of course, the big problem is trust.  Why should you trust an unofficial
set of kernel packages from me?

To solve that problem, the kernel packages are reproducibly built.  If you want to,
you can clone this project, and build your own packages, and your packages
should be absolutely identical to my packages, if you build with the same version
of Slackware and the same revision of this project (see BUILDINFO.txt in each directory).

If you choose to download my packages, _please verify the signatures_:
```
gpg --verify *.asc
```
This will prove that the checksums and packages have not been
maliciously modified after they were uploaded. (Everything is automatically
signed immediately after building, using my subkey 0xD06EABC8, as described
in the [GnuPG FAQ](https://www.gnupg.org/faq/gnupg-faq.html#automated_use).)


## Reproducible Building

For a discussion of the concept of reproducible building, see
https://reproducible-builds.org/

Reproducible building of the Linux kernel is performed by setting the kbuild
variables KBUILD_BUILD_USER, KBUILD_BUILD_HOST and KBUILD_BUILD_TIMESTAMP as
described in https://www.kernel.org/doc/Documentation/kbuild/kbuild.txt

Reproducible packaging for Slackware is performed by a modified version of
makepkg that is included in this repository.  The modifications are:
  * sort the symlinks section of doinst.sh so they appear in a consistent order
  * support the [SOURCE_DATE_EPOCH](https://reproducible-builds.org/specs/source-date-epoch/)
    environment variable to ensure all build artifacts in the package have a consistent date-time
  * make the tar-1.13 command read a sorted list of files so they are packaged
    in a consistent order
  * (unrelated to reproducible packaging) '-c y' now allows you to run makepkg
    (and the whole build process) as a non-root user

Builds must be reproduced in the same environment. This includes using the
same version of everything in the toolchain, but also, for example, if
distcc is used some build artifacts will be different from a native build.


## Building your own packages

You need to clone this repository, and then clone the Linux stable kernel
repository into a subdirectory:
```
git clone git://gitlab.com:idlemoor/dusk.git
cd dusk
git clone git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git
```
Now you can build your own packages by running dusk.SlackBuild.
You can provide an executable script named 'post-build-hook.sh'
to be run when new packages have been built -- see 'post-build-hook.sh.example'
for details.

To automate checking and building, you can use cron.  This crontab entry would check and build twice a day:
```
00 6,18 * * * (date; cd /path/to/linux-stable; git fetch --all; cd ..; sh dusk.SlackBuild) >> ~/dusk.log 2>&1
```
